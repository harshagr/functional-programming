This is the definition of the expression type.


> module Expr where
>
> import Control.Applicative
>
> data Expr = C      Double
>           | UMinus Expr
>           | Plus   Expr    Expr
>           | Minus  Expr    Expr
>           | Mul    Expr    Expr
>           | Div    Expr    Expr
>


The evaluation function. If there is a division by zero, it returns Nothing.

> eval :: Expr -> Maybe Double
> eval (C      c  ) = Just c
> eval (UMinus e  ) = fmap negate $ eval e
> eval (Plus   a b) = (+) <$> eval a <*> eval b
> eval (Minus  a b) = (-) <$> eval a <*> eval b
> eval (Mul    a b) = (*) <$> eval a <*> eval b
> eval (Div    a b) = case eval b of
>                          Nothing -> Nothing
>                          Just 0  -> Nothing
>                          Just v  -> (/v) <$> eval a
